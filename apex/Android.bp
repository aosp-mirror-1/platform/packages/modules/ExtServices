// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_team: "trendy_team_android_rubidium",
    // See: http://go/android-license-faq
    // A large-scale-change added 'default_applicable_licenses' to import
    // all of the 'license_kinds' from "packages_modules_ExtServices_license"
    // to get the below license kinds:
    //   SPDX-license-identifier-Apache-2.0
    default_applicable_licenses: ["packages_modules_ExtServices_license"],
}

apex {
    name: "com.android.extservices_tplus",
    apex_available_name: "com.android.extservices",
    defaults: ["com.android.extservices-defaults"],
    apps: ["ExtServices-tplus"],
    manifest: "tplus_apex_manifest.json",
    variant_version: "3",
    min_sdk_version: "33",
}

apex {
    name: "com.android.extservices",
    defaults: ["com.android.extservices-defaults"],
    apps: ["ExtServices-sminus"],
    java_libs: ["android.ext.adservices"],
    jni_libs: ["libtflite_support_classifiers_native"],
}

apex_defaults {
    name: "com.android.extservices-defaults",
    defaults: ["r-launched-apex-module"],
    manifest: "apex_manifest.json",
    key: "com.android.extservices.key",
    certificate: ":com.android.extservices.certificate",
    prebuilts: ["current_sdkinfo"],
    file_contexts: ":com.android.extservices-file_contexts",
    // Indicates that pre-installed version of this apex can be compressed.
    // Whether it actually will be compressed is controlled on per-device basis.
    compressible: true,
}

apex_key {
    name: "com.android.extservices.key",
    public_key: "com.android.extservices.avbpubkey",
    private_key: "com.android.extservices.pem",
}

android_app_certificate {
    name: "com.android.extservices.certificate",
    certificate: "com.android.extservices",
}
